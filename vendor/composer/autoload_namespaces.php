<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'Barryvdh' => array($vendorDir . '/barryvdh/reflection-docblock/src'),
);
