
FROM php:7.4-fpm-alpine as base

# Install system dependencies

RUN apk add --no-cache \
      freetype \
      libjpeg-turbo \
      libpng \
      curl \
      git \
      freetype-dev \
      libjpeg-turbo-dev \
      libpng-dev \
      zip \
      unzip \
    && docker-php-ext-configure gd \
      --with-freetype=/usr/include/ \
      # --with-png=/usr/include/ \ # No longer necessary as of 7.4; https://github.com/docker-library/php/pull/910#issuecomment-559383597
      --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-enable gd \
    && apk del --no-cache \
      freetype-dev \
      libjpeg-turbo-dev \
      libpng-dev \
    && rm -rf /tmp/*

RUN apk add libzip-dev

RUN docker-php-ext-install pdo pdo_mysql zip bcmath

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
# Create system mohamedessam to run Composer and Artisan Commands
RUN addgroup -S admin && adduser -D -S mohamedessam -G admin -u1000
RUN mkdir -p /home/mohamedessam/.composer && \
    chown -R mohamedessam:admin .

# Set working directory
WORKDIR /home/mohamedessam

#copying only those files since docker operates on layers so making the base layer including composer files to not have to rebuild the whole image later if the app code changed since this layer(composer) will be cached later.
COPY composer.json composer.json
COPY composer.lock composer.lock
RUN composer install  -n --no-progress --ignore-platform-reqs --no-dev --prefer-dist --no-scripts --no-autoloader

FROM base as app

ENV APP_ENV prod
ENV APP_DEBUG 0

USER mohamedessam

COPY . .

# Recreate boostrap/cache/compiled.php
RUN php artisan optimize:clear

RUN php artisan cache:clear

CMD ["php-fpm"]

# Migrate any database changes
# RUN php artisan migrate

FROM nginx:1.23.3-alpine as nginx

WORKDIR /home/mohamedessam

COPY . .