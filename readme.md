# GITLAB CI/CD for laravel project

Deployed laravel app on AWS EKS

## Contents

*   [Tools used?](#Tools-used)
*   [Bonus Features](#Bonus-Features)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
    *   [Usage](#usage)

## Tools used:

*   [x] **Docker** for building the image
*   [x] **Terraform** IAAC
*   [x] **Kubernetes** for hosting the application on ec2 instances
*   [x] **AWS** cloud provider 
*   [x] **EKS** for managing kubernetes cluster
*   [x] **ECR** for hosting the images (container registry)
*   [x] **GITLAB** for CI/CD

## BonusFeatures:

Docker image:

* Secure with **alpine** based images for it being light (faster building) and have a smaller attack surface
* **Caching** is used for even faster build (CI build took less than 2 minutes)

![Diagram](docs/CI-takes-1min-34sec.png)

* Run as **nonroot**

**Terraform**

* Use Terraform as IAAC to easily provision/destroy infra and benifit from IAAC.

**Kubernetes Role and Rolebinding**

* Instead of creating the cluster using the same user to access (which is not going to happen in real scenarios), created the cluster using another role and created the user with ability to do some actions including applying the deployment as suggested in the casestudy (all listed under deploy-infra/k8s/role.yaml)


## Getting Started

Now let's see how can we deploy this amazing app!!.

### Requirements

* Gitlab Account
* AWS account
* Install Terraform (i used different versions between 1.1.0 and 0.14.4 depending on the need, you can check the providers.tf)
* Some skills in Terraform Docker,Gitlab,Kubernetes and AWS services (ECR, EKS AND IAM).

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/MohamedEssam4444/new-laravel-project.git
```

### Usage

```bash
# Clone the repo
git clone https://gitlab.com/MohamedEssam4444/new-laravel-project.git

# Deploy infrastructure first using terraform:

cd deploy-infra/terraform/tf-state 
# Comment out the providers backend and apply s3 and dynamo db
terraform init && terraform apply 
# Remove the comments to add s3 and dynamodb to the state
terraform init && terraform apply

# Create ecr repos for app and nginx
cd ../ecr
terraform init && terraform apply 

# Create VPC,subnets,nodegroups and EKS cluster terraform modules
cd ../eks
terraform init && terraform apply

Create user sandbox in AWS and copy AWS Keys and secrets and add it in variables in gitlab along with awsregion

# Create the IAM policy for user sandbox
cd ../iam
terraform init && terraform apply
```


```bash

# Kubernetes yaml pre-deploy

# Create the role and rolebinding and map the aws user created with the cluster and the role
cd ../../k8s
kubectl apply -f role.yaml
kubectl apply -f rolebinding.yaml
eksctl create iamidentitymapping --cluster cluster --region=us-east-1 \
    --arn arn:aws:iam::577800157336:user/sandbox --username eks-user-access-role --group eks-user-access-group \
    --no-duplicate-arns

```

![Diagram](docs/map-sandbox-user-with-the-cluster-and-the-role.png)


```bash

#Gitlab

Push the repo to your gitlab account and try running the pipeline

# please check .gitlab-ci.yaml for understanding the pipeline and the project , i included some comments in

#prevent pipeline from completion if test failed:
```

![Diagram](docs/failed-test-won’t-allow-MR-to-complete.png)


```bash
Create a merge request to master
```

## Merge the request and shows here that the deploy-code job is triggered with the build stage when branch is merged to master

![Diagram](docs/deploy-when-there-is-a-merge-to-master-branch.png)



## check the k8s deployment/pod running using kubectl or any UI

![Diagram](docs/Lens-(k8s-UI)-showing-deployment-running.png)

![Diagram](docs/pod-is-running.png)

## Exec into the pod using kubectl command or LENS/K9S UI and serve on port 8080

![Diagram](docs/served-the-app-on-port-8080.png)

## App is working!!

![Diagram](docs/port-forwarded-the-pod-and-the-app-is-working-fine.png)


