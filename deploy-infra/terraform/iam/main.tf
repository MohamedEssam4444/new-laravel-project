resource "aws_iam_user_policy" "laravel-app" {
  name = "laravel-app"
  user = "sandbox"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
         "Effect":"Allow",
         "Action":[
            "ecr:ListImages"
         ],
         "Resource":[
         "${data.terraform_remote_state.ecr_repo.outputs.ecr_arn_app}",
         "${data.terraform_remote_state.ecr_repo.outputs.ecr_arn_nginx}"
         ]
      },
      {
         "Sid":"GetAuthorizationToken",
         "Effect":"Allow",
         "Action":[
            "ecr:GetAuthorizationToken"
         ],
         "Resource":"*"
      },
      {
         "Sid":"ManageRepositoryContents",
         "Effect":"Allow",
         "Action":[
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetRepositoryPolicy",
                "ecr:DescribeRepositories",
                "ecr:ListImages",
                "ecr:DescribeImages",
                "ecr:BatchGetImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:PutImage"
         ],
            "Resource":[
         "${data.terraform_remote_state.ecr_repo.outputs.ecr_arn_app}",
         "${data.terraform_remote_state.ecr_repo.outputs.ecr_arn_nginx}"
         ]
      },
        {
            "Effect": "Allow",
            "Action": "iam:CreateServiceLinkedRole",
            "Resource": "arn:aws:iam::577800157336:role/aws-service-role/eks.amazonaws.com/AWSServiceRoleForAmazonEKS",
            "Condition": {
                "ForAnyValue:StringEquals": {
                    "iam:AWSServiceName": "eks"
                }
            }
        },
        {
            "Effect": "Allow",
            "Action": "iam:PassRole",
            "Resource": "${data.terraform_remote_state.eks_role.outputs.cluster_iam_role_arn}"
        },
        {
            "Effect": "Allow",
            "Action": [
                "eks:DescribeCluster",
                "eks:ListClusters",
                "eks:AccessKubernetesApi"
            ],
            "Resource": "*"
        },
                {
            "Effect": "Allow",
            "Action": "eks:CreateCluster",
            "Resource": "arn:aws:eks:us-east-1:577800157336:cluster/cluster"
        },
        {
            "Effect": "Allow",
            "Action": [
              "ec2:Describe*",
              "ec2:StartInstances",
              "ec2:StopInstances",
              "ec2:RunInstances",
              "ec2:AssociateIamInstanceProfile",
              "ec2:ReplaceIamInstanceProfileAssociation",
              "ec2:CreateNetworkInterface",
              "ec2:DeleteNetworkInterface",
              "ec2:AssignPrivateIpAddresses",
              "ec2:UnassignPrivateIpAddresses",
              "ec2:CreateSnapshot*",
              "ec2:DeleteSnapshot"
          ],
          "Resource": "*"
        }
    ]
  })
}