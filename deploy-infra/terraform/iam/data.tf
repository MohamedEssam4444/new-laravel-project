data "terraform_remote_state" "eks_role" {
  backend = "s3"
  config = {
    bucket = "laravel-app-terraform-state"
    key    = "eks/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "ecr_repo" {
  backend = "s3"
  config = {
    bucket = "laravel-app-terraform-state"
    key    = "ecr/terraform.tfstate"
    region = "us-east-1"
  }
}