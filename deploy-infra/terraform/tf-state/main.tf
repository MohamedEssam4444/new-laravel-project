resource "aws_s3_bucket" "my-terraform-state" {
  bucket = "laravel-app-terraform-state"
  acl    = "private"
}

resource "aws_dynamodb_table" "my-terraform-lock" {
  name           = "my-terraform-lock"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}