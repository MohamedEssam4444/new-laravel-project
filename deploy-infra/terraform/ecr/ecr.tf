resource "aws_ecr_repository" "laravel_app" {
  name                 = "laravel/app"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_repository" "laravel_nginx" {
  name                 = "laravel/nginx"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}