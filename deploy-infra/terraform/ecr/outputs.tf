output "ecr_arn_app" {
  description = "IAM role ARN of the EKS cluster."
  value       = aws_ecr_repository.laravel_app.arn
}

output "ecr_arn_nginx" {
  description = "IAM role ARN of the EKS cluster."
  value       = aws_ecr_repository.laravel_nginx.arn
}