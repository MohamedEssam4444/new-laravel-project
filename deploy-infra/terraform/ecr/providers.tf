terraform {
  required_version = "~> 0.14.4"
  required_providers {
    aws = {
      version = "~> 3.74.2"
    }
  }
}
provider "aws" {
  region = "us-east-1"
}

terraform {
 backend "s3" {
   region         = "us-east-1"
   bucket         = "laravel-app-terraform-state"
   key            = "ecr/terraform.tfstate"
   dynamodb_table = "my-terraform-lock"
   encrypt        = true
 }
}