module "vpc" {
    source = "cloudposse/vpc/aws"
    # Cloud Posse recommends pinning every module to a specific version
    version     = "2.0.0"
    ipv4_primary_cidr_block = "10.0.0.0/16"

  }

  module "subnets" {
    source = "cloudposse/dynamic-subnets/aws"
    # Cloud Posse recommends pinning every module to a specific version
    version     = "2.1.0"

    availability_zones   = ["us-east-1a","us-east-1b"]
    vpc_id               = module.vpc.vpc_id
    igw_id               = [module.vpc.igw_id]
    ipv4_cidr_block           = [module.vpc.vpc_cidr_block]
    nat_gateway_enabled  = true
    nat_instance_enabled = false
  }

  module "eks_node_group" {
    source = "cloudposse/eks-node-group/aws"
    # Cloud Posse recommends pinning every module to a specific version
    version     = "2.9.0"

    desired_size                       = var.desired_size
    subnet_ids                         = module.subnets.public_subnet_ids
    min_size                           = var.min_size
    max_size                           = var.max_size
    cluster_name                       = module.eks_cluster.eks_cluster_id

    # Enable the Kubernetes cluster auto-scaler to find the auto-scaling group
    cluster_autoscaler_enabled = var.autoscaling_policies_enabled

    # Ensure the cluster is fully created before trying to add the node group
    module_depends_on = module.eks_cluster.kubernetes_config_map_id
  }

  module "eks_cluster" {
    source = "cloudposse/eks-cluster/aws"
    # Cloud Posse recommends pinning every module to a specific version
    version = "2.6.0"
    vpc_id     = module.vpc.vpc_id
    subnet_ids = module.subnets.public_subnet_ids

    region = "us-east-1"

    kubernetes_version    = "1.24"
    oidc_provider_enabled = true

  }