terraform {
  required_version = "~> 1.1.0"
  required_providers {
    aws = {
      version = "~> 4.48.0"
    }
    kubernetes = {
      version = "2.7.1"
    }
  }
}
provider "aws" {
  region = "us-east-1"
}

terraform {
 backend "s3" {
   region         = "us-east-1"
   bucket         = "laravel-app-terraform-state"
   key            = "eks/terraform.tfstate"
   dynamodb_table = "my-terraform-lock"
   encrypt        = true
 }
}