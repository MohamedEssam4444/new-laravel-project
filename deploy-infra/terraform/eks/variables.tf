variable "desired_size" {
  type        = number
  description = "Initial desired number of worker nodes (external changes ignored)"
  default = "2"
}

variable "max_size" {
  type        = number
  description = "Maximum number of worker nodes"
  default = "4"
}


variable "min_size" {
  type        = number
  description = "Minimum number of worker nodes"
  default = "1"
}

variable "autoscaling_policies_enabled" {
  type        = bool
  description = "Set true to label the node group so that the [Kubernetes Cluster Autoscaler](https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/cloudprovider/aws/README.md#auto-discovery-setup) will discover and autoscale it"
  default     = true
}