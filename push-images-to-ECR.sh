DOCKER_REGISTRY=577800157336.dkr.ecr.us-east-1.amazonaws.com
ECR_REPO=577800157336.dkr.ecr.us-east-1.amazonaws.com/laravel
WEB_APP_IMAGE=$ECR_REPO/app
NGINX_IMAGE=$ECR_REPO/nginx

aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $DOCKER_REGISTRY

docker build --build-arg BUILDKIT_INLINE_CACHE=1    \
		--cache-from ${WEB_APP_IMAGE}:master            \
		--cache-from ${TAG_LATEST_APP}    \
		--target app                               \
		-t ${TAG_LATEST_APP}              \
		-t  ${TAG_COMMIT_APP} .

docker build --build-arg BUILDKIT_INLINE_CACHE=1    \
		--cache-from ${NGINX_IMAGE}:master            \
		--cache-from ${TAG_LATEST_NGINX}    \
		--target nginx                             \
	    -t ${TAG_LATEST_NGINX}              \
		-t  ${TAG_COMMIT_NGINX} .

docker push $TAG_LATEST_APP
docker push $TAG_COMMIT_APP
docker push $TAG_COMMIT_NGINX
docker push $TAG_LATEST_NGINX